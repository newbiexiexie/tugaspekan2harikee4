<?php
    require('animal.php');
    require('ape.php');
    require('frog.php');
    $sheep = new animal("shaun");
    
    echo "Nama Hewan : " . $sheep->name; // "shaun"
    echo"<br>";
    echo "Jumlah kaki : " . $sheep->legs; // 4
    echo"<br>";
    echo "Berdarah Dingin? : " . $sheep->cold_blooded . "<br>"; // "no"
    echo"<br>";

    $sungokong = new Ape("kera sakti");
    echo "Nama Hewan : " . $sungokong->name; // "shaun"
    echo"<br>";
    echo "Jumlah kaki : " . $sungokong->legs; // 2
    echo"<br>";
    $sungokong->yell();
    echo"<br>";
    echo"<br>";

    $kodok = new Frog("buduk");
    echo "Nama Hewan : " . $kodok->name;
    echo"<br>";
    echo "Jumlah kaki : " . $kodok->legs;
    echo"<br>";
    $kodok->jump(); 
    echo"<br>";
    echo"<br>";
?>
